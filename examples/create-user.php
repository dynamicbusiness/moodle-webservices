<?php

	require_once('../ws_config.php');

	$functionname = 'core_user_create_users';

	/*
		Must include a unique email address
		Can add multiple users to the $users array
	*/

	$user1 = new stdClass();
	$user1->username = 'testuser1';
	$user1->password = 'Dynamic1$';
	$user1->firstname = 'Test';
	$user1->lastname = 'User 1';
	$user1->email = 'testuser1@email.com';
	$user1->auth = 'manual';
	$user1->idnumber = 'testuser1';
	$user1->lang = 'en';
	$user1->customfields = array(
		array('type'=>'jobrole','value'=> 'Manager'),
		array('type'=>'area','value'=> 'North'),
		array('type'=>'dept','value'=> 'Huma Resources')
	);

	$users = array($user1);
	$params = array('users' => $users);

	/// REST CALL
	header('Content-Type: text/plain');
	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
	require_once('../curl.php');
	$curl = new curl;
	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	$resp = $curl->post($serverurl . $restformat, $params);
	print_r($resp);

?>