<?php

	require_once('../ws_config.php');

	$functionname = 'core_user_update_users';

	/*
		Must send database id of user, therefore external system must store these
		OR can use the core_user_get_users_by_field function to retrieve a user by
		username,idnumber or email

		No specific return value if success, just standard RESPONSE tags.
		Error message if failed.
		
	*/

	$user1 = new stdClass();
	$user1->id = 2144;
	$user1->customfields = array(
		array('type'=>'jobrole','value'=> 'Head Supervisor'),
		array('type'=>'area','value'=> 'North-East'),
		array('type'=>'dept','value'=> 'Human Resources')
	);

	$users = array($user1);
	$params = array('users' => $users);

	/// REST CALL
	header('Content-Type: text/plain');
	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
	require_once('../curl.php');
	$curl = new curl;
	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	$resp = $curl->post($serverurl . $restformat, $params);
	print_r($resp);

?>