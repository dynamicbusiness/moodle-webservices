<?php

	require_once('../ws_config.php');

	$functionname = 'core_completion_get_course_completion_status';

	/*
		Returns completion status for a user/course.
		status: Complete = 1 / Not complete = 0
		timecompleted: timestamp of when course was completed.
		
	*/

	$data = new stdClass();
	$data->userid = 2142;
	$data->courseid = 22;


	// $course= array($course);
	$params = $data;

	/// REST CALL
	header('Content-Type: text/plain');
	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
	require_once('../curl.php');
	$curl = new curl;
	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	$resp = $curl->post($serverurl . $restformat, $params);
	print_r($resp);

?>