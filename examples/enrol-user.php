<?php

	require_once('../ws_config.php');

	$functionname = 'enrol_manual_enrol_users';

	/*
		Roleid for our purpose will typically be 5 (student role).

		The external application must store Moodle's database id for the course OR can use
		core_course_get_courses function to get all courses including database id.
		
	*/

	$enrolment = new stdClass();
	$enrolment->userid = 2143;
	$enrolment->roleid = 5; // Student role
	$enrolment->courseid = 22;


	$enrolments= array($enrolment);
	$params = array('enrolments' => $enrolments);

	/// REST CALL
	header('Content-Type: text/plain');
	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
	require_once('../curl.php');
	$curl = new curl;
	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	$resp = $curl->post($serverurl . $restformat, $params);
	print_r($resp);

?>