<?php

	require_once('../ws_config.php');

	$functionname = 'core_course_get_contents';

	/*
		Returns course contents including direct url to activity. Better to link directly into course
		rather than to the activity.
		
	*/

	$course = new stdClass();
	$course->courseid = 22;


	// $course= array($course);
	$params = $course;

	/// REST CALL
	header('Content-Type: text/plain');
	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
	require_once('../curl.php');
	$curl = new curl;
	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	$resp = $curl->post($serverurl . $restformat, $params);
	print_r($resp);

?>