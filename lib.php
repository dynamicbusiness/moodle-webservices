<?php

	require_once('ws_config.php');


	function get_courses(){
		$params = array();
		$response = call_moodle( 'core_course_get_courses', $params);

		$courses = xmlresponse_parse_courses($response);

		return $courses;


	}

	/* 
    Returns Moodle's response as a string containing XML.
	*/ 
	 function call_moodle( $function_name, $params){
	  	global $domainname,$token,$restformat;

	  	$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$function_name;


	    require_once( './curl.php' );
	    $curl = new curl;

	    $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
	    $response = $curl->post( $serverurl . $restformat, $params );

	    if (TRACING) {
	    	echo "Response from $function_name: \n", $response, "\n";
	  	}

	    return $response;
	  }


    function xmlresponse_parse_courses( $xml_string ){
	    $xml_tree = new SimpleXMLElement( $xml_string ); 
	    // echo "<pre>";var_dump($xml_tree );die;
	    $struct = new StdClass();
	    $all_courses = array();
	    foreach($xml_tree->MULTIPLE->SINGLE as $course){
	    	$tmp_arr = array();
	    	$tmp_arr['id'] = (string)$course->KEY[0]->VALUE;
	    	$tmp_arr['shortname'] = (string)$course->KEY[1]->VALUE;
	    	$tmp_arr['title'] = (string)$course->KEY[4]->VALUE;
	    	$tmp_arr['idnumber'] = (string)$course->KEY[5]->VALUE;
	    	$tmp_arr['type'] = 'e-learning';
	    	$all_courses[] = $tmp_arr ;

		}
	    return $all_courses;
  	}



?>